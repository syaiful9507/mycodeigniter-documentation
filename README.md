#DOCUMENTATION
># Dasar Perintah Codeigniter melalui Composer
>_menginstal Codeigniter 4 melalui composer, dapat dilakukan dengan mengetikan perintah berikut pada Terminal/ Command Prompt_

* [ ]   `composer create-project codeigniter4/appstarter ci-project --no-dev` *
>Anda harus menginstal codeigniter 4 di dalam root web server Anda.
>Perintah diatas akan membuat sebuah folder “ci-project” pada “xampp/htdocs”.
Setelah itu ketikan perintah berikut:
* [ ]  `composer update --no-dev`
>Selesai
># 

